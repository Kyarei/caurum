# Caurum

Makes golden tools slower at mining fragile blocks such as budding amethyst and mob spawners,
making them useful for excavating places such as amethyst geodes.

The list of blocks affected is controlled by the `caurum:fragile_blocks` block tag, and the list
of items that will have their mining speeds affected is controlled by the `caurum:careful_miners`
item tag.

Currently, the mining speed multiplier is hardcoded to be 0.2, but this might be made configurable
if there’s enough demand for it.
