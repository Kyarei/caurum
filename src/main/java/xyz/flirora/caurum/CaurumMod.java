package xyz.flirora.caurum;

import net.fabricmc.api.ModInitializer;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaurumMod implements ModInitializer {
    // This logger is used to write text to the console and the log file.
    // It is considered best practice to use your mod id as the logger's name.
    // That way, it's clear which mod wrote info, warnings, and errors.
    public static final Logger LOGGER = LoggerFactory.getLogger("caurum");

    public static final TagKey<Block> FRAGILE_BLOCKS = TagKey.of(RegistryKeys.BLOCK, new Identifier("caurum", "fragile_blocks"));
    public static final TagKey<Item> CAREFUL_MINERS = TagKey.of(RegistryKeys.ITEM, new Identifier("caurum", "careful_miners"));

    @Override
    public void onInitialize() {
        // This code runs as soon as Minecraft is in a mod-load-ready state.
        // However, some things (like resources) may still be uninitialized.
        // Proceed with mild caution.

        LOGGER.info("I command to thee: Never break another budding amethyst by mistake!");
    }
}