package xyz.flirora.caurum.mixin;

import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import xyz.flirora.caurum.CaurumMod;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity {
	@Shadow @Final private PlayerInventory inventory;

	protected PlayerEntityMixin(EntityType<? extends LivingEntity> entityType, World world) {
		super(entityType, world);
	}

	@Inject(at = @At("RETURN"), method = "getBlockBreakingSpeed", cancellable = true)
	private void adjustBlockBreakingSpeed(BlockState block, CallbackInfoReturnable<Float> cir) {
		float baseSpeed = cir.getReturnValueF();
		if (block.isIn(CaurumMod.FRAGILE_BLOCKS) && this.inventory.getMainHandStack().isIn(CaurumMod.CAREFUL_MINERS)) {
			cir.setReturnValue(baseSpeed * 0.2f);
		}
	}
}